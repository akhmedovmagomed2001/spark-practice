import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.hadoop.conf.Configuration
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.Queue
import java.net.URI
import org.apache.spark.rdd.RDD


object DataConfigs extends Serializable {
	val SCHEMA = "hdfs://localhost:9000"
	val DATA_LOCATION = s"${SCHEMA}/spark/data/"
	val OUTPUT_LOCATION = s"${SCHEMA}/spark/output/"
	val INDEX_OUTPUT_LOCATION = s"${SCHEMA}/spark/output/index_result"
}


class WordPosition (
	val position : Int,
	val filename : String,
	val wordsNear : ListBuffer[String] = new ListBuffer[String],
) extends Serializable {
	def addWordNear(word: String) = {
		wordsNear += word
	}

	override def toString() : String = {
		val sentence = wordsNear.mkString(" ")
		return s"${filename}@${position}: ${sentence}"
	}
}


class Word (
	val value: String,
	val positions : ListBuffer[WordPosition] = new ListBuffer[WordPosition],
) extends Serializable {
	def addPosition(position: WordPosition) = {
		positions += position
	}

	override def toString() : String = {
		val positionsJoined = positions.mkString("', '");
		val positionsString = s"['${positionsJoined}']"
		return s"'${value}': ${positionsString}"
	}
}


object WordUtilities extends Serializable {
	val CHARS_TO_FILTER: List[String] = List("", ",", ".", ":", "-", ";", "\n", ")", "(", "\"", "\'", "“", "”", "!", "?", "`", "_", "—", "’", "‘")

	def clearLine(line: String) : String = {
		var result = line

		for (char <- CHARS_TO_FILTER) {
			result = result.replace(char, "")
		}

		return result.toLowerCase()
	}

	def mergeWords(word1: Word, word2: Word) : Word = {
		word2.positions.foreach(position => word1.addPosition(position = position))
		return word1
	}
}


object FileUtilities extends Serializable {
	def getFilesFromDir(filesLocation: String, schema: String) : List[Path] = {
		val configuration = new Configuration()
		val fs = FileSystem.get(new URI(schema), configuration)

		val path = new Path(filesLocation)
		return fs.listStatus(path).filter(_.isFile()).map(_.getPath()).toList
	}

	def countWords(filePath: Path, outputDirFilepath: String) : String = {
		val rawFilename = filePath.getName().replace(".", "_") 
		val filename = s"${rawFilename}_count_result"
		val outputFilepath = s"${outputDirFilepath}${filename}"

		sc.textFile(filePath.toString())
			.map(line => WordUtilities.clearLine(line))
			.flatMap(line => line.split(" "))
			.map(word => word.trim())
			.filter(word => !word.isEmpty())
			.map(word => (word,1))
			.reduceByKey(_ + _)
			.sortBy(tuple => tuple._2, false)
			.map(tuple => s"${tuple._1},${tuple._2}")
			.saveAsTextFile(outputFilepath)

		return outputFilepath
	}
}


class FileIndexService(val filePath: Path) extends Serializable {
	private var _wordCounter = 0
	private val _wordsQueue = new Queue[String]()
	private val MAX_QUEUE_LENGTH = 5

	def indexFile() : RDD[Word] = {
		return sc.textFile(filePath.toString())
			.map(line => WordUtilities.clearLine(line))
			.flatMap(line => line.split(" "))
			.map(word => word.trim())
			.filter(word => !word.isEmpty())
			.map(wordRaw => toWord(wordRaw))
			.map(word => (word.value, word))
			.reduceByKey((word1, word2) => WordUtilities.mergeWords(word1, word2))
			.map(tuple => tuple._2)
	}

	def toWord(wordRaw: String) : Word = {
		val filename = filePath.getName()
		val position = new WordPosition(position = _wordCounter, filename = filename)

		if (_wordsQueue.length == MAX_QUEUE_LENGTH) {
			position.wordsNear ++= _wordsQueue
			position.wordsNear += wordRaw
			_wordsQueue.dequeue()
		}

		val word = new Word(value = wordRaw)
		word.addPosition(position)

		_wordsQueue.enqueue(wordRaw)
		_wordCounter += 1
		return word
	}
}


def index_files() : String = {
	FileUtilities.getFilesFromDir(DataConfigs.DATA_LOCATION, DataConfigs.SCHEMA)
		.map(file => new FileIndexService(file))
		.map(service => service.indexFile())
		.reduce((x, y) => x ++ y)
		.map(word => (word.value, word))
		.reduceByKey((word1, word2) => WordUtilities.mergeWords(word1, word2))
		.map(tuple => tuple._2)
		.map(word => word.toString())
		.saveAsTextFile(DataConfigs.INDEX_OUTPUT_LOCATION)
	
	return DataConfigs.INDEX_OUTPUT_LOCATION
}


def count_words() : List[String] = {
	return FileUtilities.getFilesFromDir(DataConfigs.DATA_LOCATION, DataConfigs.SCHEMA)
		.map(inputFilePath => FileUtilities.countWords(inputFilePath, DataConfigs.OUTPUT_LOCATION))
}

